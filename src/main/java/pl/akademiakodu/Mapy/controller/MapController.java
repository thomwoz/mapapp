package pl.akademiakodu.Mapy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.akademiakodu.Mapy.Coordinates;

@Controller
public class MapController {

    @GetMapping("/")
    public String inputCityName(){
        return "map/add";
    }

    @GetMapping("/result")
    public String getCoor(@RequestParam String address, ModelMap modelMap){
        modelMap.put("coordinates", Coordinates.getCoordinates(address));
        modelMap.put("url", Coordinates.getMapFromAddress(address));
        return "map/result";
    }
}
